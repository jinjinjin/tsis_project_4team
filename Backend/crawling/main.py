import dbModule as db
import crawl
import logging

logging.basicConfig(filename='./crawler.log', level=logging.DEBUG)

logging.debug("크롤러 시작")
print("크롤러 시작")

if __name__ == '__main__':
    db_class = db.Database()
    query = "select lecture_id, lecture_title, lecture_url from lecture "
    result = db_class.executeAll(query)
    for i in result:
        logging.debug("id = " + str(i['lecture_id']))

        flag = crawl.canRegister(i['lecture_url'], i['lecture_title'])
        logging.debug("크롤러 결과 0:수강불가, 1:수강가능 || " + str(flag))
        print("id = ", str(i['lecture_id']), i['lecture_url'], str(flag))
        if flag == 1:
            updateQuery = "UPDATE lecture SET valid_yn = 1 WHERE lecture_id = " + str(i['lecture_id'])
        elif flag == 0:
            updateQuery = "UPDATE lecture SET valid_yn = 0 WHERE lecture_id = " + str(i['lecture_id'])
        result = db_class.execute(updateQuery)
        db_class.commit()

logging.debug("크롤러 종료")
print("크롤러 종료")