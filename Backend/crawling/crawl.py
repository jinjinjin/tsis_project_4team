import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import logging

logging.basicConfig(filename='./crawler.log', level=logging.DEBUG)

# 셀레니움 초기 세팅
options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('window-size=1920x1080')
options.add_argument("disable-gpu")

#넘어오는 함수는 강좌목록 id, 세부 url

#메인페이지는 없는 페이지로 예외처리해두기


# 사이트별 함수
def canRegister(url, lectureName):
    webpage = requests.get(url)
    soup = BeautifulSoup(webpage.content, "html.parser")

    if url.find("www.multicampus.com") != -1: #멀티캠퍼스
        logging.debug("멀티캠퍼스 url | " + url)
        className = "detail-top__period"
        result = soup.find("div", attrs={'class': className})
        if result is not None:
            return 1
        else:
            return 0

    elif url.find("estudy.kitri.re.kr") != -1:  # 한국정보기술연구원
        logging.debug("한국정보기술연구원 url | " + url)
        className = "btnArea"
        result = soup.find("div", attrs={'class': className}).p.a.get_text()
        if result.find("가능"):
            return 1
        else:
            return 0

    elif url.find("https://edu.kosta.or.kr/lectures/search?searchText=") != -1:  # 한국소프트웨어기술진흥협회

        logging.debug("한국소프트웨어기술진흥협회 url | " + url)
        text = lectureName
        url = ""
        if text[-4:] == "(무료)" or text[-4:] == "(유료)":
            print(text[-4:])
            url = 'https://edu.kosta.or.kr/lectures/search?searchText=' + text[-4:]
        else:
            url = 'https://edu.kosta.or.kr/lectures/search?searchText=' + text

        driver = webdriver.Chrome('chromedriver', options=options)
        driver.get(url)
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        className = "text-left ng-binding"
        result = soup.find("td", attrs={'class': className})

        if result is not None:
            return 1
        else:
            return 0
    elif url.find("www.flane.co.kr") != -1:  # 패스트레인정보통신학원
        logging.debug("패스트레인정보통신학원 url | " + url)
        className = "schedule"
        result = soup.find("div", attrs={'class': className})

        try:
            if result.ol.li is not None:
                return 1
            else:
                return 0
        except AttributeError as e:
            logging.error("패스트레인정보통신학원 error | ")
            logging.error(e)
            return -1

    elif url.find("fastcampus.co.kr") != -1:  # 패스트캠퍼스
        logging.debug("패스트캠퍼스 url | " + url)

        result = soup.find("title").get_text()

        if result.find("요청된 페이지를 찾을 수 없습니다. | 패스트캠퍼스") == -1:
            return 1
        else:
            return 0

    # 중앙정보처리학원 필요없어서 skip 메인페이지만
    elif url.find("www.choongang.co.kr") != -1:
        logging.debug("중앙정보처리학원 url | " + url)

        if url == "http://www.choongang.co.kr/html/sub02_03.php?mcode=31":
            return 0
        else:
            return 1

    # 비트교육센터 skip 메인페이지만
    elif url.find("www.bitacademy.com") != -1:
        logging.debug("비트교육센터 url | " + url)
        if url == "http://www.bitacademy.com/":
            return 0
        else:
            return 1

    elif url.find("www.multicampus.com") != -1:  # HP교육센터
        logging.debug("HP교육센터 url | " + url)
        className = "btn label label_bg_yellow"
        result = soup.find("a", attrs={'class': className})

        if result is None:
            #완료 메세지 없을때
            className1 = "border-t mt20 lecture_simple_list lecture_schedule_list"
            result1 = soup.find("ul", attrs={'class': className1})
            if result1.li.get_text().find("해당 목록이 존재하지 않습니다"):
                return 0 #수강마감
            else:
                return 1
        else:
            #완료 메세지 나올때, 수강마감
            return 0 #수강마감

    elif url.find("www.oraclejava.co.kr") != -1:  # 오라클자바교육센터
        logging.debug("오라클자바교육센터 url | " + url)
        className = "color:red;"
        result = soup.find("span", attrs={'style': className})

        if result is None:
            return 1
        else:
            return 0

     # 라이지움 skip 메인페이지만
    elif url.find("www.lyzeum.com") != -1:
        logging.debug("라이지움 url | " + url)
        if url == "https://www.lyzeum.com/index.asp":
            return 0
        else:
            return 1
    # 한국정보보호교육센터 skip
    elif url.find("www.kisec.com") != -1:
        logging.debug("한국정보보호교육센터 url | " + url)
        if url == "https://www.kisec.com/home":
            return 0
        else:
            return 1


    return -1 # 이상종료

