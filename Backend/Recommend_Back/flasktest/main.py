from flask import Flask, request, send_file, render_template
import module.dbModule as db
from flask_cors import CORS
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app)

@app.route('/', methods = ['GET'])
def test():
    db_class = db.Database()
    query = "select * from survey"
    result = db_class.executeAll(query)

    for item in result:
        item['lang'] = item['lang'].split(',')
        item['trend'] = item['trend'].split(',')
        print(item['lang'])
        print(item['trend'])
    return {"data": result}

@app.route('/ispossible', methods = ['GET'])
def isPossibleToEnrollCourse():
    db_class = db.Database()
    query = "select * from test"
    result = db_class.executeAll(query)
    print(result)
    return {"data": result}

@app.route('/insert', methods = ['POST'])
def insert():
    result = request.get_json()
    print(result)
    db_class = db.Database()
    query = f'''INSERT INTO recommend.survey(emp_id, duty, lang, level, trend)
            VALUES(\'{result['28105555']}\',
            \'{result['duty']}\', \'{result['lang']}\', 
            \'{result['level']}\',\'{result['trend']}\')'''
    print(query)
    result = db_class.execute(query)
    db_class.commit()
    return {"data": result}

# @app.route('/inserttest', methods = ['POST'])
# def inserttest():
#     result = request.get_json()
#     strtest = f'{result['lang']}'.split(',')
#     print("쪼개지나요")
#     print(strtest)
#     print(result)
#     db_class = db.Database()
#   query = f'''INSERT INTO recommend.survey(employeeID, name, duty, lang, level, trend)
#            VALUES(\'{result['employeeID']}\',
#             \'{result['name']}\', \'{result['duty']}\', \'{result['lang']}\',
#             \'{result['level']}\',\'{result['trend']}\')'''
#     print(query)
#     result = db_class.execute(query)
#     db_class.commit()
#     return {"data": result}

@app.route('/update', methods = ['GET'])
def update():
    db_class = db.Database()
    query = "UPDATE recommend.test SET id = 10 WHERE id = 3"
    result = db_class.execute(query)
    db_class.commit()
    return {"data": result}

@app.route('/delete', methods = ['GET'])
def delete():
    db_class = db.Database()
    query = "ALTER recommend.test DROP age;"
    result = db_class.execute(query)
    db_class.commit()
    return {"data": result}
'''
@app.route('/todayProduce', methods = ['GET'])
def todayProduce():
    #test1.test111.__init__(self='')
    #result = test1.test11()
    result = todayPrices.__init__(self='')
    print(result)
    return result

@app.route('/todayProduceWithout', methods = ['GET'])
def todayProduceWithout():
    allergies = request.args.get('allergies')
    vegi = request.args.get('vegi')
    print("들어온 알러지", allergies)
    print("들어온 베지타입", vegi)
    result = produceWithout.__init__(self='', allergies=allergies, vegi=vegi)
    print(result)
    return result

@app.route('/produceImg', methods = ['GET'])
def produceImg():
    path_dir = './static/data/img/'
    file_list = os.listdir(path_dir)  # path 에 존재하는 파일 목록 가져오기
    name = request.args.get('name')
    print("넘어온 name =", name)
    findname = "-"
    for f in file_list:
        if f.split(".")[0] in name:
            findname = f
            print("f는", f , "name은 ",name)
        if name == f.split(".")[0]:
            return send_file(path_dir + f, mimetype='image/jpg')
    if findname == "-":
        filename = 'error.jpg'
    else:
        filename = findname
    return send_file(path_dir+filename, mimetype='image/jpg')

'''
if __name__ == '__main__':
    app.run(debug=True)