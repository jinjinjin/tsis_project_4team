def renewtrend(num):
    import numpy as np
    import pandas as pd
    from pandas import DataFrame
    from sklearn.metrics.pairwise import cosine_similarity
    import pymysql
    import csv
    import module.dbModule as db
    import __main__

    user = pd.read_csv('./user.csv', index_col=0)
    interest = pd.read_csv('./interest.csv', index_col=0)

    db_class = db.Database()

    query = f"select * from survey where emp_id={num}"
    data_list = db_class.executeOne(query)
    
    if data_list['trend'] == '':
        pass
    else:
        trends = data_list['trend'].split(', ')
    
    interest.loc[data_list['emp_id']] = 0

    for x in trends:
        interest.loc[data_list['emp_id'], str(x)] = 1

    user.loc[int(data_list['emp_id'])] = 0
    user[int(data_list['emp_id'])] = 0
    user = user.fillna(0)

    a = [interest.iloc[-1].to_numpy()]
    b = interest.iloc[:-2].to_numpy()
    sim = cosine_similarity(a, b)

    for i in range(len(sim[0])):
        user.iloc[-1, i] = sim[0][i]
        user.iloc[i, -1] = sim[0][i]


    x = num
    result = np.where(np.isin(user.loc[int(x)].values, user.loc[int(x)].max()))

    interest.to_csv('interest.csv')
    user.to_csv('user.csv')
    return __main__.select_recommend(user.columns[result[0][0]])