from flask import Flask, request, send_file, render_template
import module.dbModule as db
import renew_lang
import renew_trend
import renew_user
from flask_cors import CORS
import crawling
import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.metrics.pairwise import cosine_similarity
import pymysql
import csv

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app)

@app.route('/api/flask/renewLang', methods = ['POST'])
def renewLang():
    result = request.get_json()
    emp_id = result['emp_id']

    userlang = pd.read_csv('./userlang.csv', index_col=0)
    test = np.where(np.isin(userlang.loc[int(emp_id)].values, userlang.loc[int(emp_id)].max()))
    x = userlang.columns[test[0][0]]
    db_class = db.Database()
    query = f'''SELECT lec.lecture_id, lec.lecture_title, lec.lecture_url,
                        lec.valid_yn, lec.online_yn, lec.lecture_best_yn,
                        lec.category_id, lec.academy_id, lec.edu_level_id,
                        lec.theme_lecture_id, lec.academy_subject_id,
                        acdm.academy_name, acdm.academy_logo_url
                FROM lecture lec
                INNER JOIN academy acdm
                ON lec.academy_id = acdm.academy_id
                WHERE lecture_id IN (
                    SELECT lecture_id 
                    FROM wishlist 
                    WHERE emp_id = {x});'''
    result = db_class.executeAll(query)
    db_class.close()
    return {"data": result}


@app.route('/api/flask/renewTrend', methods = ['POST'])
def renewTrend():
    result = request.get_json()
    emp_id = result['emp_id']

    user = pd.read_csv('./user.csv', index_col=0)
    test = np.where(np.isin(user.loc[int(emp_id)].values, user.loc[int(emp_id)].max()))
    x = user.columns[test[0][0]]
    db_class = db.Database()
    query = f'''SELECT lec.lecture_id, lec.lecture_title, lec.lecture_url,
                        lec.valid_yn, lec.online_yn, lec.lecture_best_yn,
                        lec.category_id, lec.academy_id, lec.edu_level_id,
                        lec.theme_lecture_id, lec.academy_subject_id,
                        acdm.academy_name, acdm.academy_logo_url
                FROM lecture lec
                INNER JOIN academy acdm
                ON lec.academy_id = acdm.academy_id
                WHERE lecture_id IN (
                    SELECT lecture_id 
                    FROM wishlist 
                    WHERE emp_id = {x});'''
    result = db_class.executeAll(query)
    db_class.close()
    return {"data": result}

@app.route('/api/flask/renewUser', methods = ['POST'])
def renewUser():
    result = request.get_json()
    emp_id = result['emp_id']

    user = pd.read_csv('./user_user.csv', index_col=0)
    test = np.where(np.isin(user.loc[int(emp_id)].values, user.loc[int(emp_id)].max()))
    x = user.columns[test[0][0]]
    db_class = db.Database()
    query = f'''SELECT lec.lecture_id, lec.lecture_title, lec.lecture_url,
                        lec.valid_yn, lec.online_yn, lec.lecture_best_yn,
                        lec.category_id, lec.academy_id, lec.edu_level_id,
                        lec.theme_lecture_id, lec.academy_subject_id,
                        acdm.academy_name, acdm.academy_logo_url
                FROM lecture lec
                INNER JOIN academy acdm
                ON lec.academy_id = acdm.academy_id
                WHERE lecture_id IN (
                    SELECT lecture_id 
                    FROM wishlist 
                    WHERE emp_id = {x});'''
    result = db_class.executeAll(query)
    db_class.close()
    return {"data": result}

def select_recommend(id):
    db_class = db.Database()
    emp_id = id
    query = f'SELECT * FROM lecture WHERE lecture_id IN (SELECT lecture_id FROM wishlist WHERE emp_id = {emp_id});'
    result = db_class.executeAll(query)
    print(result)
    db_class.close()
    return {"data": result}

#top10 news from https://www.itworld.co.kr/main/
@app.route('/api/flask/top10news', methods = ['GET'])
def top10news():
    result = crawling.news10()
    return {"data": result}

@app.route('/api/flask/top5', methods = ['GET'])
def top5():
    data = pd.read_csv('./interest.csv')
    top = data.sum()[1:]
    top = top.sort_values(ascending=False)
    index = top[:5].index.tolist()
    values = top[:5].values.tolist()

    data_list = {} 
    for x in range(5):
        data_list[index[x]] = values[x] 

    return {"data": data_list}

@app.route('/api/flask/recommend', methods = ['GET'])
def recommend():
    db_class = db.Database()
    user = pd.read_csv('./user_user.csv', index_col=0)
    test = np.where(np.isin(user.loc[int(emp_id)].values, user.loc[int(emp_id)].max()))
    x = user.columns[test[0][0]]
    emp_id = request.args.get('emp_id')
    query = f'''SELECT lec.lecture_id, lec.lecture_title, lec.lecture_url,
                        lec.valid_yn, lec.online_yn, lec.lecture_best_yn,
                        lec.category_id, lec.academy_id, lec.edu_level_id,
                        lec.theme_lecture_id, lec.academy_subject_id,
                        acdm.academy_name, acdm.academy_logo_url
                FROM lecture lec
                INNER JOIN academy acdm
                ON lec.academy_id = acdm.academy_id
                WHERE lecture_id IN (
                    SELECT lecture_id 
                    FROM wishlist 
                    WHERE emp_id = {x});'''

    result = db_class.executeAll(query)
    db_class.close()
    return {"data": result}

@app.route('/api/flask/insert', methods = ['POST'])
def insert():
    result = request.get_json()
    db_class = db.Database()
    query = "INSERT INTO test_tsis_edu.survey(emp_id, duty, lang, level, trend) \
            VALUES('"+ result['emp_id']+ "', \
            '"+ result['duty']+ "', \
            '"+result['lang'] +"', \
            '"+result['level']+"', \
            '"+result['trend']+"')"
    param = result['emp_id'] 
    result = db_class.execute(query)
    db_class.commit()
    
    renew_lang.renewlang(param)
    renew_trend.renewtrend(param)
    renew_user.renewuser(param)

    db_class.close()
    return "data"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8001,debug=True)
