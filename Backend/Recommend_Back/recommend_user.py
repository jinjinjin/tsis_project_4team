#!/usr/bin/env python
# coding: utf-8

# In[35]:


# pip install numpy, pandas, sklearn
import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.metrics.pairwise import cosine_similarity
import pymysql
import csv

data = pd.read_csv('./survey.csv', encoding='cp949')
info = data[['귀하의 사번', '1-1. (개발 직무자) 귀하가 사용하는 언어는?(최대 3개)', '1 -2. 귀하가 원하는 과정 수준은?','2. 최근 관심있는 IT 트랜드는?']]
info = info.set_index('귀하의 사번')
info = pd.DataFrame(info)
info = info.fillna('')

people = pd.DataFrame(index=info.index, columns = ['JAVA','C / C++','C#','Python','웹(자바스크립트, HTML, CSS 등)','기타','기초과정','전문과정','IoT','SW공학','기술관리','네트워크','데이터베이스','데이터사이언스','디자인','보안','블록체인','빅데이터','세미나','시스템','운영체제','인공지능','클라우드','품질','프로그래밍','IT프로젝트관리','핀테크'])
people.index.name = '사번'
people = people.fillna(0)

for i, row in info.iterrows():
    for items in row.values:
        items = items.replace('웹(자바스크립트, HTML, CSS 등)', '웹(자바스크립트/ HTML/ CSS 등)')
        if items == '':
            pass
        else:
            items = items.split(', ')
        for item in items:
            if item == '웹(자바스크립트/ HTML/ CSS 등)':
                item = '웹(자바스크립트, HTML, CSS 등)'
            people.loc[i, item] = 1

user = DataFrame(index=people.index, columns=people.index)
user = user.fillna(0)

for i in range(len(people)-1):
    a = [people.iloc[i].to_numpy()]
    b = people.iloc[i+1:].to_numpy()
    sim = cosine_similarity(a, b)
    k = 0
    for j in range(i+1, len(people)-1):
        user.iloc[i, j] = sim[0][k]
        user.iloc[j, i] = sim[0][k]
        k += 1


people.to_csv('user_people.csv')
user.to_csv('user_user.csv')


# In[ ]:




