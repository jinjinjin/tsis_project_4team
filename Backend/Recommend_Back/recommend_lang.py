#!/usr/bin/env python
# coding: utf-8

# In[7]:


# pip install numpy, pandas, sklearn, pymysql
import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.metrics.pairwise import cosine_similarity
import pymysql
import csv

data = pd.read_csv('./survey.csv', encoding='cp949')
infolang = data[['귀하의 사번', '1-1. (개발 직무자) 귀하가 사용하는 언어는?(최대 3개)']]
infolang = infolang.set_index('귀하의 사번')
infolang = pd.DataFrame(infolang)
infolang = infolang.fillna('')

lang = pd.DataFrame(index=infolang.index, columns = ['JAVA','C / C++','C#','Python','웹(자바스크립트, HTML, CSS 등)','기타'])
lang.index.name = '사번'
lang = lang.fillna(0)
            
for i, row in infolang.iterrows():
    for items in row.values:
        items = items.replace('웹(자바스크립트, HTML, CSS 등)', '웹(자바스크립트/ HTML/ CSS 등)')
        if items == '':
            pass
        else:
            items = items.split(', ')
        for item in items:
            if item == '웹(자바스크립트/ HTML/ CSS 등)':
                item = '웹(자바스크립트, HTML, CSS 등)'
            lang.loc[i, item] = 1

userlang = DataFrame(index=lang.index, columns=lang.index)
userlang = userlang.fillna(0)

for i in range(len(lang)-1):
    a = [lang.iloc[i].to_numpy()]
    b = lang.iloc[i+1:].to_numpy()
    sim = cosine_similarity(a, b)
    k = 0
    for j in range(i+1, len(lang)):
        userlang.iloc[i, j] = sim[0][k]
        userlang.iloc[j, i] = sim[0][k]
        k += 1
        
lang.to_csv('lang.csv')
userlang.to_csv('userlang.csv')

