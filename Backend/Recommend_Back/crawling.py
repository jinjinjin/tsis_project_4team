import requests
from bs4 import BeautifulSoup


def news10():
    url = "https://www.itworld.co.kr/main/"
    webpage = requests.get(url)
    soup = BeautifulSoup(webpage.content, "html.parser")

    className = "news_list_has_thumb_size news_list_title default_font color_deep_gray font_bold font_limit title_h3"
    result = soup.findAll("h4", attrs={'class': className})

    data = {}
    news = list()
    for i in result[:10]:
        data['title'] = i.get_text().strip('\n')
        data['html'] = url+i.a['href'].strip('\n')
        print(data)
        data = {}
        news.append(data)
    print("tt", news)
    return news

