#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# pip install numpy, pandas, sklearn, pymysql
def test(num):
    import numpy as np
    import pandas as pd
    from pandas import DataFrame
    from sklearn.metrics.pairwise import cosine_similarity
    import pymysql
    import csv
    import module.dbModule as db
    import __main__

    userlang = pd.read_csv('./userlang.csv', index_col=0)
    lang = pd.read_csv('./lang.csv', index_col=0)

    db_class = db.Database()
    query = f"select * from survey where emp_id={num}"
    data_list = db_class.executeOne(query)

    #langs = data_list['lang'].split(', ')
    if data_list['lang'] == '':
        pass
    else:
        langs = data_list['lang'].split(', ')
                
    lang.loc[data_list['emp_id']] = 0

    for x in langs:
        lang.loc[data_list['emp_id'], str(x)] = 1
    # print(lang)

    userlang.loc[int(data_list['emp_id'])] = 0
    userlang[int(data_list['emp_id'])] = 0
    userlang = userlang.fillna(0)

    a = [lang.iloc[-1].to_numpy()]
    b = lang.iloc[:-2].to_numpy()
    sim = cosine_similarity(a, b)

    for i in range(len(sim[0])):
        userlang.iloc[-1, i] = sim[0][i]
        userlang.iloc[i, -1] = sim[0][i]

    #print(userlang)
    #x = int(input("사번을 입력하세요:"))
    x = num
    # print(x)
    # print((userlang.loc[x].values))
    result = np.where(np.isin(userlang.loc[int(x)].values, userlang.loc[int(x)].max()))
    
    #print(userlang.columns[result[0][0]])

    lang.to_csv('lang.csv')
    userlang.to_csv('userlang.csv')

    return __main__.select_lang(userlang.columns[result[0][0]])

