# pip install numpy, pandas, sklearn, pymysql
import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.metrics.pairwise import cosine_similarity
import pymysql
import csv

data = pd.read_csv('./survey.csv', encoding='cp949')
info = data[['귀하의 사번', '2. 최근 관심있는 IT 트랜드는?']]
info = info.set_index('귀하의 사번')
info = pd.DataFrame(info)

interest = pd.DataFrame(index=info.index, columns = ['IoT','SW공학','기술관리','네트워크','데이터베이스','데이터사이언스','디자인','보안','블록체인','빅데이터','세미나','시스템','운영체제','인공지능','클라우드','품질','프로그래밍','IT프로젝트관리','핀테크'])
interest.index.name = '사번'
interest = interest.fillna(0)

for i, row in info.iterrows():
    for items in row.values:
        items = items.split(', ')
        for item in items:
            interest.loc[i, item] = 1

user = DataFrame(index=interest.index, columns=interest.index)
user = user.fillna(0)

for i in range(len(interest)-1):
    a = [interest.iloc[i].to_numpy()]
    b = interest.iloc[i+1:].to_numpy()
    sim = cosine_similarity(a, b)
    k = 0
    for j in range(i+1, len(interest)):
        user.iloc[i, j] = sim[0][k]
        user.iloc[j, i] = sim[0][k]
        k += 1

interest.to_csv('interest.csv')
user.to_csv('user.csv')
