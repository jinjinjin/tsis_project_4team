# pip install numpy, pandas, sklearn, pymysql
def renewuser(num):
    import numpy as np
    import pandas as pd
    from pandas import DataFrame
    from sklearn.metrics.pairwise import cosine_similarity
    import pymysql
    import csv
    import module.dbModule as db
    import __main__

    user = pd.read_csv('./user_user.csv', index_col=0)
    people = pd.read_csv('./user_people.csv', index_col=0)
    print(people)
    db_class = db.Database()

    query = f"select * from survey where emp_id={num}"
    data_list = db_class.executeOne(query)
    
    people.loc[data_list['emp_id']] = 0

    #lang
    if data_list['lang'] == '':
        pass
    else:
        langs = data_list['lang'].split(', ')

    for x in langs:
        people.loc[data_list['emp_id'], str(x)] = 1

    #level
    if data_list['level'] == '':
        pass
    else:
        level = data_list['level']

    people.loc[data_list['emp_id'], str(x)] = 1

    #trend
    if data_list['trend'] == '':
        pass
    else:
        trends = data_list['trend'].split(', ')
    for x in trends:
        people.loc[data_list['emp_id'], str(x)] = 1   

    #print(people)    
    user.loc[int(data_list['emp_id'])] = 0
    user[int(data_list['emp_id'])] = 0
    user = user.fillna(0)

    a = [people.iloc[-1].to_numpy()]
    b = people.iloc[:-2].to_numpy()
    sim = cosine_similarity(a, b)

    for i in range(len(sim[0])):
        user.iloc[-1, i] = sim[0][i]
        user.iloc[i, -1] = sim[0][i]

    x = num
    result = np.where(np.isin(user.loc[int(x)].values, user.loc[int(x)].max()))

    people.to_csv('user_people.csv')
    user.to_csv('user_user.csv')
    return __main__.select_recommend(user.columns[result[0][0]])





