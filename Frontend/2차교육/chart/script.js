let labels1 = ['빅데이터', '블록체인', 'AI', '정보보안', '데이터베이스'];
let data1 = [22, 14, 10, 8, 3];
let colors1 = ['#0094d4', '#1d378b', '#6c6d6f', '#9c9ea0', '#00BFFF'];

let myDoughnutChart = document
    .getElementById("myChart")
    .getContext('2d');

let chart1 = new Chart(myDoughnutChart, {
    type: 'doughnut',
    data: {
        labels: labels1,
        datasets: [
            {
                data: data1,
                backgroundColor: colors1
            }
        ]
    },
    options: {
        title: {
            // text: "Do you like doughnuts?", display: true
        },
        legend: {
            // position: 'chartArea' alias: 'start'
            boxwidth: 300,
            maxwidth: true
        }
    }
});
